<?php

namespace Drupal\user_instance\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\Role;
use Drupal\Core\Render\Element\Checkboxes;

class UserInstanceSettingsForm extends FormBase {

    public function getFormId()
    {
        return 'user_instance_settings_form';
    }

    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $form ['user_count'] = array(
            '#type' => 'textfield',
            '#required' => TRUE,
            '#title' => $this->t('Number of user you want to create.')

        );
        $form ['user_password'] = array(
            '#type' => 'textfield',
            '#required' => TRUE,
            '#title' => $this->t('Login Password'),
            '#description' => $this->t('Password to login into the account.')

        );
        $roles = Role::loadMultiple();
        $roles_list = array();
        foreach ($roles as $role => $roleObj) {
            $roles_list[$role] = $roleObj->get('label');
        }
        unset($roles_list['anonymous'], $roles_list['authenticated']);

        $form['user_role'] = array(
            '#type' => 'checkboxes',
            '#options' => $roles_list,
            '#title' => $this->t('Roles you want to assign to the user.')
        );
        $form['user_status'] = array(
            '#type' => 'radios',
            '#default_value' => 1,
            '#options' => array(
                0 => $this->t('Blocked'),
                1 => $this->t('Active'),
            ),
            '#title' => $this->t('Status.')
        );
        $form['user_instance_submit'] = array(
            '#type' => 'submit',
            '#value' => $this->t('Create User'),
        );
        return $form;
    }

    public function validateForm(array &$form, FormStateInterface $form_state)
    {
        if (!is_numeric($form_state->getValue('user_count'))) {
            $form_state->setErrorByName('user_count', $this->t('User count should be in numeric'));
        }
    }

    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        $user_count = $form_state->getValue('user_count');
        $user_status = $form_state->getValue('user_status');
        $user_password = $form_state->getValue('user_password');
        $user_roles = Checkboxes::getCheckedCheckboxes($form_state->getValue('user_role'));
        for ($i = 0; $i < $user_count; $i++) {
            $values = array();
            $values = array(
                'name' => 'test-' . strtotime('now'),
                'mail' => 'test' . strtotime('now') . '@test-' . strtotime('now') . '.com',
                'roles' => $user_roles,
                'pass' => $user_password,
                'status' => $user_status
            );
            $account = entity_create('user', $values);
            $account->save();
        }
    }
}